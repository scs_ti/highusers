from functions import *
from querys import *
from sqlalchemy.types import INTEGER
from datetime import datetime
from dateutil.relativedelta import relativedelta
from tqdm import tqdm

# %% PARAMENTROS DE CONEXÕES AOS BANCO DE DADOS
user_dw,  psw_dw, host_dw = get_paramentros_con('prd_db_provedor.db')

user_oracle,  psw_oracle, host_oracle = get_paramentros_con('prdme.db')

# PARAMETROS
engine_dw = get_engine_dw()
last_month = datetime.now() - relativedelta(months=1)
last2 = int(last_month.strftime('%Y%m'))
curmonth = int(datetime.now().strftime('%Y%m'))

competencias = pd.read_sql('select t.competencia from dim_tempo t where t.cd_tempo between {} and {}'.format(last2,curmonth), con=engine_dw)

print('Deletando dados dos ultimos 2 meses da tabela highuser')
with engine_dw.connect() as con:
    result = con.execute('delete FROM highuser t where t.ANO_MES_INICIO >= {}'.format(last2))
print('deletado com sucesso.')

for i in tqdm(range(len(competencias))):
    competencia = competencias['competencia'].iloc[i]
    anomes = int(competencia[3:7]+competencia[0:2])
    sql_formatada = sql.format(competencia,anomes)
    with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as con:
        df_result = pd.read_sql(sql_formatada, con)
    df_result.to_sql('highuser', con=engine_dw, if_exists='append', index=False,
                          dtype={"ANO_MES_INICIO": INTEGER})


data_execucao = datetime.today().strftime('%d/%m/%Y %H:%M:00')
data = datetime.strptime(data_execucao,'%d/%m/%Y %H:%M:00')

with engine_dw.connect() as con:
    con.execute('''DELETE FROM historico_execucao H WHERE H.script = 'highuser' ''')
    con.execute('''INSERT INTO historico_execucao (data_execucao, script) VALUES ('{}', 'highuser')'''.format(data))
