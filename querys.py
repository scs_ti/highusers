sql = '''
SELECT A.CD_MATRICULA                                                    AS "MATRICULA",
       U.NM_SEGURADO                                                     AS "BENEFICIARIO",
       CASE WHEN U.SN_TITULAR = 'S' THEN 'Titular' ELSE 'Dependente' END AS "TITULARIDADE",
       SUM(A.QT_COBRADO)                                                 AS "QT_COBRADO",
       ROUND(SUM(A.VL_TOTAL_PAGO), 2)                                    AS "DESPESA",
       TRUNC((SYSDATE - U.DT_NASCIMENTO) / 365.25)                       AS "IDADE",
       U.DT_NASCIMENTO                                                   AS "DT_NASCIMENTO",
       CASE WHEN U.SN_ATIVO = 'S' THEN 'Ativo' ELSE 'Inativo' END        AS "ATIVO",
       CASE
           WHEN
                   (SELECT 1
                    FROM DBAPS.USUARIO Q
                    WHERE Q.DT_CADASTRO <= SYSDATE
                      AND Q.CD_MATRICULA = A.cd_matricula
                      AND NOT EXISTS(SELECT 1
                                     FROM DBAPS.DESLIGAMENTO D
                                     WHERE D.CD_MATRICULA = Q.CD_MATRICULA
                                       AND D.DT_REATIVACAO IS NULL
                                       AND D.DT_DESLIGAMENTO <= SYSDATE)) IS NOT NULL THEN 'Ativo'
           ELSE 'Inativo' END                                            AS "ATIVO_ATUALMENTE",
       U.CD_PLANO || ' - ' || PLA.DS_PLANO                               AS "PLANO",
       CASE WHEN U.TP_SEXO = 'M' THEN 'Masculino' ELSE 'Feminino' END    AS "SEXO",
       U.NM_CIDADE                                                       AS "CIDADE",
       '{0}'                                                         AS "COMP_INICIAL",
       '{0}'                                                         AS "COMP_FINAL",
       {1}                                                        AS "ANO_MES_INICIO"                                                             
FROM DBAPS.V_CTAS_MEDICAS A
         LEFT JOIN DBAPS.PRESTADOR B ON A.CD_PRESTADOR_PRINCIPAL = B.CD_PRESTADOR
         LEFT JOIN DBAPS.GUIA C ON A.NR_GUIA = C.NR_GUIA
         LEFT JOIN DBAPS.PRESTADOR_ENDERECO D ON C.CD_PRESTADOR_ENDERECO = D.CD_PRESTADOR_ENDERECO
         LEFT JOIN DBAPS.ESPECIALIDADE E ON C.CD_ESPECIALIDADE = E.CD_ESPECIALIDADE
         LEFT JOIN DBAPS.PROCEDIMENTO F ON A.CD_PROCEDIMENTO = F.CD_PROCEDIMENTO
         INNER JOIN DBAPS.REPASSE_PRESTADOR RP ON RP.CD_REPASSE_PRESTADOR = A.CD_REPASSE_PRESTADOR
         LEFT JOIN DBAPS.PRESTADOR P ON P.CD_PRESTADOR = C.CD_PRESTADOR_EXECUTOR AND RP.CD_PRESTADOR = A.CD_PRESTADOR_PAGAMENTO
         INNER JOIN DBAPS.PAGAMENTO_PRESTADOR PP ON RP.CD_PAGAMENTO_PRESTADOR = PP.CD_PAGAMENTO_PRESTADOR
         INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = A.CD_MATRICULA
         LEFT JOIN DBAPS.PLANO PLA ON PLA.CD_PLANO = U.CD_PLANO
WHERE A.TP_SITUACAO_CONTA IN ('AA', 'AT')
  AND A.TP_SITUACAO_ITCONTA IN ('AA', 'AT')
  AND A.TP_SITUACAO_EQUIPE IN ('AA', 'AT')
  AND A.TP_ORIGEM = '2'
  AND RP.CD_CON_PAG IS NOT NULL
  AND A.DT_COMPETENCIA >= SUBSTR(('{0}'), 4, 7) || SUBSTR(('{0}'), 0, 2)
  AND A.DT_COMPETENCIA <= SUBSTR(('{0}'), 4, 7) || SUBSTR(('{0}'), 0, 2)
  AND A.VL_TOTAL_PAGO > 0
GROUP BY A.CD_MATRICULA,
         U.NM_SEGURADO,
         CASE WHEN U.SN_TITULAR = 'S' THEN 'Titular' ELSE 'Dependente' END,
          TRUNC((SYSDATE - U.DT_NASCIMENTO) / 365.25),
           U.DT_NASCIMENTO,
         CASE WHEN U.SN_ATIVO = 'S' THEN 'Ativo'   ELSE 'Inativo' END,
         U.CD_PLANO || ' - ' || PLA.DS_PLANO,
         CASE WHEN U.TP_SEXO = 'M' THEN 'Masculino' ELSE 'Feminino' END,
         U.NM_CIDADE,
         '{0}',
         '{0}'
ORDER BY 5 DESC
'''